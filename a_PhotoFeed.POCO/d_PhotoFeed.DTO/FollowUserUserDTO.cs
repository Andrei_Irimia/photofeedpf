﻿namespace d_PhotoFeed.DTO
{
    public class FollowUserUserDTO
    {
        public int IdFollow { get; set; }
        public int IdUser { get; set; }
        public int IdUserFollowed { get; set; }
    }
}
