﻿namespace d_PhotoFeed.DTO
{
    public class JuryDTO
    {
        public int IdJury { get; set; }
        public int IdProContest { get; set; }
        public int IdJuryUser { get; set; }

    }
}
