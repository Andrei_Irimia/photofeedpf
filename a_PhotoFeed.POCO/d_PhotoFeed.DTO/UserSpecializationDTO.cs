﻿namespace d_PhotoFeed.DTO
{
    public class UserSpecializationDTO
    {
        public int Id { get; set; }
        public int IdUserSpec { get; set; }
        public int IdSpecialization { get; set; }
    }
}
