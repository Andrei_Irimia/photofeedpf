﻿namespace d_PhotoFeed.DTO
{
    public class FeedbackRatingDTO
    {
        public int IdFeedbackRating { get; set; }
        public string Usefulness { get; set; }
    }
}
