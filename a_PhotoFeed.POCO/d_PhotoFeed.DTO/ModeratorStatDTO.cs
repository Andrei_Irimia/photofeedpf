﻿namespace d_PhotoFeed.DTO
{
    public class ModeratorStatDTO
    {
        public int IdModerator { get; set; }
        public string ModeratorRole { get; set; }

    }
}
