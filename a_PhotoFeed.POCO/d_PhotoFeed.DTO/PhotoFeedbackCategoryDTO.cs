﻿namespace d_PhotoFeed.DTO
{
    public class PhotoFeedbackCategoryDTO
    {
        public int IdPhotoFeedbackCategory { get; set; }
        public int IdCategory { get; set; }
        public int IdPhoto { get; set; }
    }
}
