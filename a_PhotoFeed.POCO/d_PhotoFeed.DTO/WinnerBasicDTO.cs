﻿namespace d_PhotoFeed.DTO
{
    public class WinnerBasicDTO
    {
        public int IdAward { get; set; }
        public int IdBasicContest { get; set; }
        public int IdWinnerUser { get; set; }
        public int PositionPlaced { get; set; }
    }
}
