﻿namespace d_PhotoFeed.DTO
{
    public class JudgeInvitationDTO
    {
        public int IdInvitation { get; set; }
        public int IdCompany { get; set; }
        public int IdUserInvited { get; set; }
        public int IdContest { get; set; }
        public int Accepted { get; set; }
    }
}
