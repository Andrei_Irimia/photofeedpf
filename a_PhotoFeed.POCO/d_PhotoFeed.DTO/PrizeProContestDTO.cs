﻿namespace d_PhotoFeed.DTO
{
    public class PrizeProContestDTO
    {
        public int IdPrize { get; set; }
        public int IdProContest { get; set; }
        public int Position { get; set; }
        public string PositionPrize { get; set; }
    }
}
