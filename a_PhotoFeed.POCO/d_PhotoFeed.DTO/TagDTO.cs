﻿namespace d_PhotoFeed.DTO
{
    public class TagDTO
    {
        public int IdTag { get; set; }
        public string TagName { get; set; }
    }
}
