﻿namespace d_PhotoFeed.DTO
{
    public class WinnerProDTO
    {
        public int IdAward { get; set; }
        public int IdProContest { get; set; }
        public int IdWinnerUser { get; set; }
        public int PositionPlaced { get; set; }
    }
}
