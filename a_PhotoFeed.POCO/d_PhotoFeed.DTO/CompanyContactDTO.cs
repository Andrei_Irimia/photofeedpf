﻿namespace d_PhotoFeed.DTO
{
    public class CompanyContactDTO
    {
        public int IdContact { get; set; }
        public int IdContactCompany { get; set; }
        public string WebsiteName { get; set; }
        public string WebsiteUrl { get; set; }
    }
}
