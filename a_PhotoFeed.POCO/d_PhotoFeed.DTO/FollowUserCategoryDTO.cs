﻿namespace d_PhotoFeed.DTO
{
    public class FollowUserCategoryDTO
    {
        public int IdFollow { get; set; }
        public int IdUser { get; set; }
        public int IdCategoryFollowed { get; set; }
    }
}
