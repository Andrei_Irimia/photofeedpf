﻿namespace d_PhotoFeed.DTO
{
    public class PositionStatDTO
    {
        public int IdPosition { get; set; }
        public string PositionName { get; set; }
    }
}
