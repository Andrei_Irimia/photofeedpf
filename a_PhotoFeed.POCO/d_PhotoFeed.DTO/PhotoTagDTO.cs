﻿namespace d_PhotoFeed.DTO
{
    public class PhotoTagDTO
    {
        public int IdPhotoTag { get; set; }
        public int IdTag { get; set; }
        public int IdPhoto { get; set; }
    }
}
