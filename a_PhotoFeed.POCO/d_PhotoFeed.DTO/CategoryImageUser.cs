﻿namespace d_PhotoFeed.DTO
{
    public class CategoryImageUser
    {
        public int UserId { get; set; }
        public int ImageId { get; set; }
        public string Username { get; set; }
        public string Image { get; set; }
        public string Category { get; set; }
    }
}
