﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace d_PhotoFeed.DTO
{
    public class UserBaseInfo
    {
        public int IdUser { get; set; }
        public string UserName { get; set; }
    }
}
