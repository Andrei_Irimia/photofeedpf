﻿namespace d_PhotoFeed.DTO
{
    public class FollowCompanyCompanyDTO
    {
        public int IdFollow { get; set; }
        public int IdCompany { get; set; }
        public int IdCompanyFollowed { get; set; }
    }
}
