﻿namespace d_PhotoFeed.DTO
{
    public class CompanyMemberDTO
    {
        public int IdCompanyMember { get; set; }
        public int IdUser { get; set; }
        public int IdCompany { get; set; }
    }
}
