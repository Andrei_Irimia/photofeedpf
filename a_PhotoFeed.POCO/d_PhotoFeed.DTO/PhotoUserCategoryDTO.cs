﻿namespace d_PhotoFeed.DTO
{
    public class PhotoUserCategoryDTO
    {
        public int IdPhotoUserCategory { get; set; }
        public int IdCategory { get; set; }
        public int IdPhoto { get; set; }
    }
}
