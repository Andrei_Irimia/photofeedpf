﻿namespace d_PhotoFeed.DTO
{
    public class CopyrightStatDTO
    {
        public int IdCopyright { get; set; }
        public int Copyright { get; set; }
    }
}
