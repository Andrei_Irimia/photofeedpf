﻿namespace d_PhotoFeed.DTO
{
    public class FollowUserCompanyDTO
    {
        public int IdFollow { get; set; }
        public int IdUser { get; set; }
        public int IdCompanyFollowed { get; set; }
    }
}
