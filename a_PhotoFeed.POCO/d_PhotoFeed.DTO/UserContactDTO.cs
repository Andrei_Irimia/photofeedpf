﻿namespace d_PhotoFeed.DTO
{
    public class UserContactDTO
    {
        public int IdContact { get; set; }
        public int IdContactUser { get; set; }
        public string WebsiteName { get; set; }
        public string WebsiteUrl { get; set; }
    }
}
