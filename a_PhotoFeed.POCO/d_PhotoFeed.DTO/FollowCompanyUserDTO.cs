﻿namespace d_PhotoFeed.DTO
{
    public class FollowCompanyUserDTO
    {
        public int IdFollow { get; set; }
        public int IdCompany { get; set; }
        public int IdUserFollowed { get; set; }
    }
}
