﻿namespace d_PhotoFeed.DTO
{
    public class SpecializationDTO
    {
        public int IdSpecialization { get; set; }
        public string SpecializationName { get; set; }
    }
}
